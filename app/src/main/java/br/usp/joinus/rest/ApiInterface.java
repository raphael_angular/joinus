package br.usp.joinus.rest;

import br.usp.joinus.model.CoordinatesResponse;
import retrofit2.Call;
import retrofit2.http.GET;


public interface ApiInterface {
    @GET("data.json/")
    Call<CoordinatesResponse> getCoord();
}