package br.usp.joinus.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CoordinatesResponse {
    @SerializedName("count")
    private int page;
    @SerializedName("next")
    private Object next;
    @SerializedName("previous")
    private Object previous;
    @SerializedName("results")
    private List<Coordinates> results;
    @SerializedName("total_results")
    private int totalResults;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public Object getNext() {
        return next;
    }

    public void setNext(Object next) {
        this.next = next;
    }

    public Object getPrevious() {
        return previous;
    }

    public void setPrevious(Object previous) {
        this.previous = previous;
    }

    public List<Coordinates> getResults() {
        return results;
    }

    public void setResults(List<Coordinates> results) {
        this.results = results;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }
}
