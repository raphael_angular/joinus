package br.usp.joinus.model;


import com.google.gson.annotations.SerializedName;

public class CoordinatesSend {

    @SerializedName("title")
    private String title;
    @SerializedName("user")
    private String user;
    @SerializedName("details")
    private String details;
    @SerializedName("lat")
    private Double lat;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    @SerializedName("lon")
    private Double lon;
    @SerializedName("date")
    private String date;
    @SerializedName("duration")
    private String duration;
    @SerializedName("starttime")
    private String starttime;



    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }
}
