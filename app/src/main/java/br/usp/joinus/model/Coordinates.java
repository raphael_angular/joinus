package br.usp.joinus.model;

import com.google.gson.annotations.SerializedName;


public class Coordinates {

    @SerializedName("title")
    private String title;
    @SerializedName("code")
    private String code;
    @SerializedName("lat")
    private Double lat;
    @SerializedName("lon")
    private Double lon;
    @SerializedName("user")
    private String user;
    @SerializedName("date")
    private String date;
    @SerializedName("duration")
    private String duration;
    @SerializedName("starttime")
    private String starttime;


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public Coordinates(String title, String code, Double lat, Double lon, String user, String date, String duration, String starttime){
        this.title = title;
        this.code = code;
        this.lat = lat;
        this.lon = lon;
        this.user = user;
        this.date = date;
        this.duration = duration;
        this.starttime = starttime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
